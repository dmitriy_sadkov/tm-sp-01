package ru.sadkov.tm.service.serviceImpl;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public abstract class AbstractService {

    @NotNull
    protected SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    protected final SimpleDateFormat simpleDateFormatForDB = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
}
