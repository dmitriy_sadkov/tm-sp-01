package ru.sadkov.tm.service.serviceImpl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.enumerate.Status;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.util.RandomUtil;

import java.util.Date;
import java.util.List;

@Service
public  class ProjectService extends AbstractService implements IProjectService {


    @NotNull
    @Autowired
    private ProjectRepository projectRepository;


    @Override
    public Project findOne(@NotNull final String projectId) {
        if(projectRepository.findById(projectId).isPresent()) return projectRepository.findById(projectId).get();
        return null;
    }

    @Override
    public void remove(@NotNull final String projectId) {
        projectRepository.deleteById(projectId);
    }

    @Nullable
    public String findProjectIdByName(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        @Nullable final Project project = projectRepository.findOneByName(projectName);
        if (project == null) return null;
        return project.getId();
    }

    @Transactional
    public boolean persist(@Nullable final String projectName, @Nullable final String description) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        if (projectRepository.existsByName(projectName)) return false;
        @NotNull final Project project = new Project(projectName, description, RandomUtil.UUID());
        projectRepository.save(project);
        return true;
    }

    @Transactional
    public void removeByName(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        @Nullable final String projectId = findProjectIdByName(projectName);
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.deleteByName(projectName);
    }

    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void update(@Nullable final String id, @Nullable final String newName, @Nullable final String description) {
        if (id == null || newName == null || id.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        projectRepository.update(id, description, newName);
    }

    public @Nullable Project findOneByName(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        @Nullable final String projectId = findProjectIdByName(projectName);
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.findOneByName(projectName);
    }

    @Override
    public @Nullable List<Project> findProjectsByPart(@Nullable final String part) {
        if (part == null || part.isEmpty()) return null;
        return projectRepository.findProjectsByPart(part);
    }

    @Override
    public @Nullable List<Project> findProjectsByStatus(@Nullable final Status status) {
        if (status == null) return null;
        return projectRepository.findProjectsByStatus(status);
    }

    @Override
    @Nullable
    @Transactional
    public String startProject(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        @NotNull final Date startDate = new Date();
        projectRepository.startProject(Status.PROCESS, projectName, startDate);
        return simpleDateFormat.format(startDate);
    }

    @Override
    @Transactional
    public @Nullable String endProject(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return null;
        @NotNull final Date endDate = new Date();
        projectRepository.endProject(Status.DONE, projectName, endDate);
        return simpleDateFormat.format(endDate);
    }

    @Override
    public @NotNull List<Project> findAll() {
       return projectRepository.findAll();
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void clear() {
      projectRepository.deleteAll();
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        clear();
        for (@NotNull final Project project : projects) {
            persist(project);
        }
    }
}
