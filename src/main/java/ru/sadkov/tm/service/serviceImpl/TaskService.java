package ru.sadkov.tm.service.serviceImpl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.model.enumerate.Status;
import ru.sadkov.tm.repository.TaskRepository;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.ITaskService;
import ru.sadkov.tm.util.RandomUtil;

import java.util.*;

@Service
public  class TaskService extends AbstractService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private IProjectService projectService;

    public @Nullable Task findTaskByName(@Nullable final String taskName) {
        if (taskName == null || taskName.isEmpty()) return null;
        return taskRepository.findOneByName(taskName);
    }

    @Transactional
    @Override
    public boolean saveTask(@Nullable final String taskName, @Nullable final String projectName, @Nullable final String description) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (projectName == null || projectName.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName);
        @Nullable final Project project = projectService.findOneByName(projectName);
        if (project == null) return false;
        if (taskRepository.existsByNameAndProjectId(taskName, projectId)) return false;
        @NotNull final Task task = new Task(RandomUtil.UUID(), taskName, project, description);
        persist(task);
        return true;
    }

    @Transactional
    public void removeTask(@Nullable final String taskName) {
        taskRepository.deleteByName(taskName);
    }

    @Nullable
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @Transactional
    public void removeTaskForProject(@Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName);
        if (projectId == null || projectId.isEmpty()) return;
        @Nullable final Project project = projectService.findOneByName(projectName);
        if (project == null) return;
        @NotNull final Iterator<Task> iterator = taskRepository.findAll().iterator();
        while (iterator.hasNext()) {
            @NotNull final Task task = iterator.next();
            if (task.getProject().getId().equals(projectId)) {
                taskRepository.deleteByName(task.getName());
            }
        }
    }

//    @Transactional
//    public void update(@Nullable final String oldName, @Nullable final String newName) {
//        if (oldName == null || oldName.isEmpty()) return;
//        if (newName == null || newName.isEmpty()) return;
//        @Nullable final Task task = findTaskByName(oldName);
//        if (task == null) return;
//        taskRepository.update(task.getId(), newName);
//    }

    @Override
    @Nullable
    public List<Task> getTasksByPart(@Nullable final String part) {
        if (part == null || part.isEmpty()) return null;
        return taskRepository.getTasksByPart(part);
    }

    @Override
    @Nullable
    public List<Task> findTasksByStatus(@Nullable final Status status) {
        return taskRepository.findAllByStatus(status);
    }

    @Override
    @Nullable
    @Transactional
    public String startTask(@Nullable final String taskName) {
        if (taskName == null || taskName.isEmpty()) return null;
        @NotNull final Date startDate = new Date();
        taskRepository.startTask(Status.PROCESS, taskName, startDate);
        return simpleDateFormat.format(startDate);
    }

    @Override
    @Nullable
    @Transactional
    public String endTask(@Nullable final String taskName) {
        if (taskName == null || taskName.isEmpty()) return null;
        @NotNull final Date endDate = new Date();
        taskRepository.endTask(Status.DONE, taskName, endDate);
        return simpleDateFormat.format(endDate);
    }

    @Override
    @Transactional
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void load(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        clear();
        for (@NotNull final Task task : tasks) {
            taskRepository.save(task);
        }
    }

    @Override
    public List<Task> findAllByProjectId(String id) {
        return taskRepository.findAllByProjectId(id);
    }

    @Override
    public Task findOneById(String id) {
        if (taskRepository.findById(id).isPresent())
            return taskRepository.findById(id).get();
        return null;
    }

    @Override
    @Transactional
    public void removeById(String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void update(String id, String name, String description) {
//        System.out.println(id);
//        System.out.println(name);
//        System.out.println(description);
        taskRepository.update(id, name, description);
    }
}

