package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.enumerate.Status;

import java.util.List;

public interface IProjectService {

    @Nullable
    String findProjectIdByName(@Nullable final String projectName);

    boolean persist(@Nullable final String projectName, @Nullable final String description);

    void removeByName(@Nullable final String projectName);


    void update(@Nullable final String id, @Nullable final String newName, @Nullable final String description);

    @Nullable
    Project findOneByName(@Nullable final String projectName);

    @Nullable
    List<Project> findProjectsByPart(@Nullable final String part);

    @Nullable
    List<Project> findProjectsByStatus(@Nullable final Status status);

    @Nullable
    String startProject(@Nullable final String projectName);

    @Nullable
    String endProject(@Nullable final String projectName);

    @NotNull
    List<Project> findAll();

    void persist(@Nullable final Project project);

    void clear();

    void load(@Nullable final List<Project> projects);

    Project findOne(@NotNull final String projectId);

    void remove(@NotNull final String projectId);
}
