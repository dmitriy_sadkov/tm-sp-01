package ru.sadkov.tm.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.model.enumerate.Status;

import java.util.Date;
import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task,String> {

    Task findOneByName(String taskName);

    boolean existsByNameAndProjectId(String taskName, String projectId);

    void deleteByName(String taskName);

    List<Task> findAll();

    @Modifying
    @Query("update Task set name = ?2, description = ?3 where id = ?1")
    void update(String id, String newName, String description);

    @Query("select t from Task t where t.name like %?1% or t.description like %?1%")
    List<Task> getTasksByPart(String part);

    List<Task> findAllByStatus(Status status);

    @Modifying
    @Query("update Task set status = ?1, dateBegin = ?3 where name = ?2")
    void startTask(Status process, String taskName, Date startDate);

    @Modifying
    @Query("update Task set status = ?1, dateEnd = ?3 where name = ?2")
    void endTask(Status done, String taskName, Date endDate);

    List<Task> findAllByProjectId(String id);
}
