package ru.sadkov.tm.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.enumerate.Status;

import java.util.Date;
import java.util.List;

@Repository
public interface ProjectRepository extends CrudRepository<Project, String> {
    List<Project> findAll();

    Project findOneByName(String projectName);

    boolean existsByName(String projectName);

    void deleteByName(String projectName);

    @Modifying
    @Query("update Project set description = ?2, name = ?3 where id = ?1")
    void update(String projectId, String description, String newName);

    @Query("select p from Project p where  p.name like %?1% or p.description like %?1%")
    List<Project> findProjectsByPart(String part);

    @Query("select p from Project p where p.status = ?1")
    List<Project> findProjectsByStatus(Status status);

    @Modifying
    @Query("update Project set status = ?1, dateBegin = ?3 where name = ?2")
    void startProject(Status process, String projectName, Date startDate);

    @Modifying
    @Query("update Project set status = ?1, dateEnd = ?3 where name = ?2")
    void endProject(Status done, String projectName, Date endDate);
}
