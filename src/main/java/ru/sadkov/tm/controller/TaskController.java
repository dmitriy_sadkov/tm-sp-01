package ru.sadkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.model.Task;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.ITaskService;

import java.util.List;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    ITaskService taskService;

    @NotNull
    @Autowired
    IProjectService projectService;


    @GetMapping("/tasks/{id}")
    public String tasks(@PathVariable("id") String id, @NotNull Model model) {
        @NotNull List<Task> taskList = taskService.findAllByProjectId(id);
        model.addAttribute("taskList", taskList);
        model.addAttribute("projectId", id);
        return "tasks";
    }

    @RequestMapping(value = "/task-create/{id}", method = RequestMethod.GET)
    public String createTaskGet(Model model, @PathVariable("id") String id) {
        model.addAttribute("projectId",id);
        return "createTask";
    }

    @RequestMapping(value = "/task-create", method = RequestMethod.POST)
    public String createTaskPost(@ModelAttribute("id") final String id, @ModelAttribute("name") final String name, @ModelAttribute("description") final String description, BindingResult result) {
        Project project = projectService.findOne(id);
        taskService.saveTask(name,project.getName(),description);
        return "redirect:/tasks/"+id;
    }

    @RequestMapping(value = "/startTask/{id}", method = RequestMethod.GET)
    public String startTask(Model model, @PathVariable("id") String id) {
        Task task = taskService.findOneById(id);
        String projectId = task.getProject().getId();
        taskService.startTask(task.getName());
        return "redirect:/tasks/"+projectId;
    }

    @RequestMapping(value = "/endTask/{id}", method = RequestMethod.GET)
    public String endTask(Model model, @PathVariable("id") String id) {
        Task task = taskService.findOneById(id);
        String projectId = task.getProject().getId();
        taskService.endTask(task.getName());
        return "redirect:/tasks/"+projectId;
    }

    @RequestMapping(value = "/viewTask/{id}", method = RequestMethod.GET)
    public String viewTask(Model model, @PathVariable("id") String id) {
        Task task = taskService.findOneById(id);
        model.addAttribute("task", task);
        return "task";
    }

    @RequestMapping(value = "/deleteTask/{id}", method = RequestMethod.GET)
    public String removeTask(@PathVariable("id") String id) {
        String projectId = taskService.findOneById(id).getProject().getId();
        taskService.removeById(id);
        return "redirect:/tasks/"+projectId;
    }


    @RequestMapping(value = "/editTask/{id}", method = RequestMethod.GET)
    public String editTask(Model model, @PathVariable("id") String id) {
        Task task = taskService.findOneById(id);
        model.addAttribute("task", task);
        return "editTask";
    }

    @RequestMapping(value = "/editTask", method = RequestMethod.POST)
    public String editTaskPost(@ModelAttribute("task") Task task,@ModelAttribute("projectId") String projectId, BindingResult result) {
        taskService.update(task.getId(), task.getName(), task.getDescription());
        return "redirect:/tasks/"+projectId;
    }
}
