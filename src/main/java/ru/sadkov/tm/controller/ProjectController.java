package ru.sadkov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.sadkov.tm.model.Project;
import ru.sadkov.tm.service.IProjectService;
import ru.sadkov.tm.service.ITaskService;

import java.util.List;

@Controller
public class ProjectController {

    @NotNull
    @Autowired
    ITaskService taskService;

    @NotNull
    @Autowired
    IProjectService projectService;


    @GetMapping("/")
    public String index(@NotNull final Model model) {
        return "index";
    }

    @GetMapping("/projects")
    public String projects(@NotNull Model model) {
        @NotNull List<Project> projects = projectService.findAll();
        model.addAttribute("projectList", projects);
        return "projects";
    }

    @RequestMapping(value = "/project-create", method = RequestMethod.GET)
    public String createProjectGet(@ModelAttribute("project") Project project,
                                   BindingResult result) {
        return "editProject";
    }

    @RequestMapping(value = "/start/{id}", method = RequestMethod.GET)
    public String startProject(Model model, @PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        projectService.startProject(project.getName());
        return "redirect:/projects";
    }

    @RequestMapping(value = "/end/{id}", method = RequestMethod.GET)
    public String endProject(Model model,@PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        projectService.endProject(project.getName());
        return "redirect:/projects";
    }

    @RequestMapping(value = "/project-create", method = RequestMethod.POST)
    public String createProjectPost(@ModelAttribute("name") final String name, @ModelAttribute("description") final String description, BindingResult result) {
        projectService.persist(name, description);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
    public String viewProject(Model model, @PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        return "project";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String removeProject(@PathVariable("id") String id) {
        projectService.remove(id);
        return "redirect:/projects";
    }

    @RequestMapping(value = "/home")
    public String backToProjectList(Model model) {
        return "redirect:/projects";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editProject(Model model, @PathVariable("id") String id) {
        Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        return "editProject";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editProjectPost(@ModelAttribute("project") Project project) {
        System.out.println("im here");
        System.out.println(project.toString());
        projectService.update(project.getId(),project.getName(),project.getDescription());
        return "redirect:/projects";
    }

    @GetMapping(value = "/developer")
    public String developer(){
        return "developer";
    }

}
