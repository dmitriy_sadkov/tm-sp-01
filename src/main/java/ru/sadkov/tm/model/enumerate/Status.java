package ru.sadkov.tm.model.enumerate;

import org.jetbrains.annotations.NotNull;

public enum Status {

    PLANNED("запланировано"),
    PROCESS("в процессе"),
    DONE("готово");

    @NotNull
    private String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}
