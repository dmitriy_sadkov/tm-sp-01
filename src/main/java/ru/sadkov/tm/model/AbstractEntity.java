package ru.sadkov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.model.enumerate.Status;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@Getter
@Setter
public abstract class AbstractEntity {

    @NotNull
    @Id
    String id;

    @NotNull
    @Column(name = "name")
    String name;

    @Nullable
    @Column(name = "description")
    String description;

    @NotNull
    @Column(name = "date_create")
    @Temporal(TemporalType.DATE)
    Date dateCreate;

    @Nullable
    @Column(name = "date_begin")
    @Temporal(TemporalType.DATE)
    Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @Temporal(TemporalType.DATE)
    Date dateEnd;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    Status status;

}
