<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.02.2020
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Projects</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
    <a class="navbar-brand" href="${pageContext.request.contextPath}">Project Manager</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/home">Projects <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/developer">Developer<span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>
<br>
<div class="container-fluid">
    <h1>Projects</h1>
    <table class="table table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Status</th>
            <th>Id</th>
            <th>DateCreate</th>
            <th>DateBegin</th>
            <th>DateFinish</th>
            <th>CRUD</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="project" items="${projectList}">
        <tr>
            <td>${project.name}</td>
            <td>${project.description}</td>
            <td>${project.status}</td>
            <td>${project.id}</td>
            <td>${project.dateCreate}</td>
            <td>${project.dateBegin}</td>
            <td>${project.dateEnd}</td>
            <td>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a class="btn btn-primary btn-sm" href="/spring-mvc/edit/${project.id}">edit</a>
                    <a class="btn btn-primary btn-sm" href="/spring-mvc/delete/${project.id}">delete</a>
                    <a class="btn btn-primary btn-sm" href="/spring-mvc/view/${project.id}">view</a>
                    <a class="btn btn-primary btn-sm" href="/spring-mvc/start/${project.id}">start</a>
                    <a class="btn btn-primary btn-sm" href="/spring-mvc/end/${project.id}">end</a>
                    <a class="btn btn-primary btn-sm" href="/spring-mvc/tasks/${project.id}">Tasks</a>
                </div>

            </td>
            </c:forEach>
        </tbody>
    </table>
    <br><br>
    <form action="project-create">
        <button class="btn btn-primary" type="submit"><b>Create Project</b></button>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>
