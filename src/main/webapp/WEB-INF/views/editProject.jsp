<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <c:if test="${empty project.id}">
        <title>Add</title>
    </c:if>
    <c:if test="${!empty project.id}">
        <title>Edit</title>
    </c:if>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
    <a class="navbar-brand" href="${pageContext.request.contextPath}">Project Manager</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/home">Projects <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/developer">Developer<span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>
<br>
<c:if test="${empty project.id}">
    <c:url value="/project-create" var="var"/>
</c:if>
<c:if test="${!empty project.id}">
    <c:url value="/edit" var="var"/>
</c:if>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm">
            <form action="${var}" method="POST">
                <c:if test="${!empty project.id}">
                    <input type="hidden" name="id" value="${project.id}">
                </c:if>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input class="form-control" placeholder="Enter name" type="text" name="name" id="name">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input class="form-control" placeholder="Enter description" type="text" name="description"
                           id="description">
                </div>
                <c:if test="${empty project.id}">
                    <button class="btn btn-primary" type="submit" value="Add Project">Add Project</button>
                </c:if>
                <c:if test="${!empty project.id}">
                    <button class="btn btn-primary" type="submit" value="Edit Project">Edit Project</button>
                </c:if>
            </form>
        </div>
        <div class="col-sm">
        </div>
        <div class="col-sm">
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>